<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\TECategory;
use App\Models\TEAuthors;
use App\Models\TEComments;
use App\Models\TECommentsLikedIP;

use App\Models\TEPosts; // sadece başlık
use App\Models\TEPostProperties;
use App\Models\TEPostTag;
use App\Models\TECategorieables;
// contents haber içerikleri
// TE\Authors\Models\Article
use App\Models\TEArticles;
use App\Models\TESlugs;
use App\Models\TEContents;
use App\Models\TERedirections;
use App\Models\TEMediaFiles;
use App\Models\TEGalleries;
use App\Models\TEGalleryMeta;
use App\Models\TEVideos;
use DateTime;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

use App\Models\Ticarihayat\HaberHaberler;
use App\Models\Ticarihayat\HaberMakale;
use App\Models\Ticarihayat\HaberGallery;
use App\Models\Ticarihayat\HaberGalleryItem;
use App\Models\Ticarihayat\HaberYazarlar;
use App\Models\Ticarihayat\HaberYorumlar;
use App\Models\Ticarihayat\HaberKategori;
use App\Models\Ticarihayat\Video;

// mavixa

class Ticarihayat extends Controller
{
    public $site = '/ticarihayat-com';

    public function transferAllDB() {
        // $this->insertAuthors();
        // $this->insertCategories();
        // $this->instertGallery();
        // $this->insertGalleryMeta();

        // $this->insertHaber();
        // $this->insertArticles();
        // $this->insertComments();
        // $this->instertVideo();
        //$this->updatePostProperties();
        $this->updateContents();


   
  
        // video için prefix - normalde videolar eklenirken eklenmeli
        /* foreach (TESlugs::where('id', '>', 140484)->get() as $key => $item) {

                $item->prefix = 'plugins.video::slug.video';
                $item->save();
            
        } */

        
        
    }

    // Yazarlar
    public function insertAuthors(){

        $character = ["Ý", "ý", "Þ", "þ", "ð"];
        $change = ["İ", "ı","Ş", "ş", "ğ"];
    
        foreach (HaberYazarlar::all() as $item) {

            if($item['Yazarad']) {
                
                $yazar_name = str_replace($character, $change, $item['Yazarad']);

                TESlugs::insert([
                    [
                        'key' =>  Str::slug($yazar_name),
                        'reference_type'=> 'TE\Authors\Models\Author',
                        'reference_id' => $item['Yazarid'] > 0 ? $item['Yazarid'] : 0,
                        'created_at' => date('Y-m-d H:i:s'),
                        'updated_at' => date('Y-m-d H:i:s')
                    ]
                ]);

                TEAuthors::insert([
                    [
                        'id'=> $item['Yazarid'],
                        'name' => str_replace($character, $change, $yazar_name),
                        'email' => $item['Yazaremail'],
                        'status' => $item['aktif'],
                        'order' => $item['yazarorder'],
                        'created_at' => date('Y-m-d H:i:s'),
                        'updated_at' => date('Y-m-d H:i:s')
                    ]
                ]);
                
            }

        }
    }

    // Kategoriler
    public function insertCategories() {

        $character = ["Ý", "ý", "Þ", "þ", "ð"];
        $change = ["İ", "ı","Ş", "ş", "ğ"];
      
        foreach (HaberKategori::all() as $item) {
                $category = str_replace($character, $change, $item['kategoriadi']);

                TERedirections::insert([
                    [
                        'from' => '/kategori/'.Str::slug($category).'/'.$item['kategoriid'],
                        'to' => '/'.Str::slug($category),
                    ]
                ]);
    
                TECategory::insert([
                    [
                        'id'=> $item['kategoriid'],
                        'name'=> $category,
                        'reference' => 'TE\Blog\Models\Post',
                        'status' => $item['aktif'],
                        'order' => 1,
                        'author_type' => '',
                        'status' => 1,
                        'description' => $item['name'],
                        'created_at' => date('Y-m-d H:i:s'),
                        'updated_at' => date('Y-m-d H:i:s')
                    ]
                ]);
    
                TESlugs::insert([
                    [
                        'key' =>  Str::slug($category),
                        'reference_type'=> 'TE\Category\Models\Category',
                        'reference_id' => $item['kategoriid'],
                        'created_at' => date('Y-m-d H:i:s'),
                        'updated_at' => date('Y-m-d H:i:s')
                    ]
                ]);

        

        }
    }


    public function insertGalleryMeta() {

        foreach (TEGalleries::all() as $key => $item) {

            $gallery_meta = [];

            foreach (HaberGalleryItem::where('galerikatid', $item->id)->get() as $k => $gallery_item) {
                
                $meta_data = [
                    "img" => $this->site."/image/".$gallery_item->galeriphoto,
                    "description" => trim($gallery_item->galeritext)
                ];

                array_push($gallery_meta, $meta_data);

            }

            if(count($gallery_meta) > 0) {
            
                TEGalleryMeta::insert([
                    [
                        'images' => json_encode($gallery_meta),
                        'reference_type'=> "TE\Gallery\Models\Gallery",
                        'reference_id'=> $item->id,
                        'created_at' => date('Y-m-d H:i:s'),
                        'updated_at' => date('Y-m-d H:i:s')
                    ]
                ]);

            }

        }

    }

    public function instertGallery() {
        $character = ["Ý", "ý", "Þ", "þ", "ð"];
        $change = ["İ", "ı","Ş", "ş", "ğ"];
        
     

        foreach (HaberGallery::all() as $key => $item) {

            $galery_title = str_replace($character, $change, $item['galeribaslik']);

          /*  TERedirections::insert([
                [
                    'from' => '/galeri/'.Str::slug($galery_title, '-').'/'.$item['galeriid'],
                    'to' => '/'.Str::slug($galery_title, '-'),
                    'status' => 'published'
                ]
            ]); 

            TEGalleries::insert([
                [
                    'id' => $item['galeriid'],
                    'name'=> $galery_title,
                    'description' => '',
                    'order' => 1,
                    'image' => $item['galeriresim'],
                    'user_id' => 0,
                    'status' => "published",
                    'created_at' => $item['galeritarih'],
                    'updated_at' => date('Y-m-d H:i:s')
                ]
            ]);

            TECategorieables::insert([
                [
                    'category_id' => 18,
                    'categorieable_type'=> "TE\Gallery\Models\Gallery",
                    'categorieable_id'=> $item['galeriid'],
                    'created_at' => $item['galeritarih'],
                    'updated_at' => date('Y-m-d H:i:s')
                ]
            ]); */

            /* if($item->id > 17957 && $item->id < 18019) {
                $item->
                $item->save();
            } */

            TESlugs::insert([
                [
                    'key' => Str::slug($galery_title, '-'),
                    'reference_type'=> "TE\Gallery\Models\Gallery",
                    'reference_id'=> $item['galeriid'],
                    'prefix' => 'plugins.gallery::slug.galleries',
                    'created_at' => $item->created_at,
                    'updated_at' => $item->update_at
                ]
            ]);
            

        }



        /* foreach (TEVideos::all() as $key => $item) {
            
            TECategorieables::insert([
                [
                    'category_id' => 19,
                    'categorieable_type'=> "TE\Video\Models\Video",
                    'categorieable_id'=> $item->id,
                    'created_at' => $item->created_at,
                    'updated_at' => $item->update_at
                ]
            ]);
            
        } */

        // galeri - video - post - makale - category herşeyin slug tablosuna aktarılması lazım
        // İLGİLİ METODLARIN İÇİNE SLUG INSERTLERİ EKLE
       
       /* foreach (TEGalleries::all() as $key => $item) {

            TESlugs::insert([
                [
                    'key' => Str::slug($item['name'], '-'),
                    'reference_type'=> "TE\Gallery\Models\Gallery",
                    'reference_id'=> $item->id,
                    'created_at' => $item->created_at,
                    'updated_at' => $item->update_at
                ]
            ]);

        } */

        /* foreach (TEVideos::all() as $key => $item) {

            TESlugs::insert([
                [
                    'key' => Str::slug($item['name'], '-'),
                    'reference_type'=> "TE\Video\Models\Video",
                    'reference_id'=> $item->id,
                    'created_at' => $item->created_at,
                    'updated_at' => $item->update_at
                ]
            ]);
        } */
        //
        
    }

    public function instertVideo() {
        $character = ["Ý", "ý", "Þ", "þ", "ð"];
        $change = ["İ", "ı","Ş", "ş", "ğ"];
        
     

        foreach (Video::all() as $key => $item) {

            $video_title = str_replace($character, $change, $item['videotitle']);

            if($video_title) {

                if($item->videoembed) {
                    $parcala_embed = explode("embed/", $item->videoembed);
            
                    if(count($parcala_embed) > 1) {
                        $url = 'https://www.youtube.com/watch?v=' . strtok($parcala_embed[1], '"');
                        $mime_type = 'youtube';
                    }
                }

                if($item->videoname) {
                    $mime_type = 'video/mp4';
                    $url = $this->site.'/video/'.$item->video_name;
                } 


                TEMediaFiles::insert([
                    [
                        'id' => $item->id,
                        'user_id' => 1,
                        'name'=> $video_title,
                        'folder_id' => 0,
                        'options' => "",
                        'size' => 0,
                        'mime_type' => $mime_type,
                        'url' => $url,
                        'created_at' => $item['videodate'] ? $item['videodate'] : date('Y-m-d H:i:s'),
                        'updated_at' => $item['videodate'] ? $item['videodate'] : date('Y-m-d H:i:s')
                    ]
                ]);

                TEVideos::insert([
                    [
                        'name'=> $video_title,
                        'description' => $item['videosummary'],
                        'is_featured'=> 0,
                        'order' => 1,
                        'headline'=> 0,
                        'story'=> 0,
                        'image' => $item['videoimage'],
                        'hide_headline'=> 0,
                        'media_id'=> $item['videoid'],
                        'user_id' => 1,
                        'embed' => $item->videoembed,
                        'status' => 'published',
                        'created_at' => $item['videodate'] ? $item['videodate'] : date('Y-m-d H:i:s'),
                        'updated_at' => $item['videodate'] ? $item['videodate'] : date('Y-m-d H:i:s')
                    ]
                ]);
  
                TERedirections::insert([
                    [
                        'from' => '/video/'.Str::slug($video_title, '-').'/'.$item['videoid'],
                        'to' => '/'.Str::slug($video_title, '-'),
                        'status' => 'published'
                    ]
                ]); 

            

                TECategorieables::insert([
                    [
                        'category_id' => 19,
                        'categorieable_type'=> "TE\Video\Models\Video",
                        'categorieable_id'=> $item['videoid'],
                        'created_at' => $item['videodate'] ? $item['videodate'] : date('Y-m-d H:i:s'),
                        'updated_at' => $item['videodate'] ? $item['videodate'] : date('Y-m-d H:i:s')
                    ]
                ]);

                TESlugs::insert([
                    [
                        'key' => Str::slug($video_title, '-'),
                        'reference_type'=> "TE\Video\Models\Video",
                        'reference_id'=> $item['videoid'],
                        'created_at' => $item['videodate'] ? $item['videodate'] : date('Y-m-d H:i:s'),
                        'updated_at' => $item['videodate'] ? $item['videodate'] : date('Y-m-d H:i:s')
                    ]
                ]);

            }
        }
   
        
    }

    public function updateContents() {
        $data = TEContents::where('id', '>', 70000)->get();
        
        $character = ["&not;", "&LTR;", "&lrm;", 
        "&ndash;", 
        "&Ucirc;", 
        "&hellip;", 
        "&icirc;", 
        "&ucirc;", "&Acirc;", "&acirc;",
        "&lsquo;", "&#39;", "&rsquo;", 
        "&Ouml;", '&ouml;', 
        '&Ccedil;', "&ccedil;",
        '&Uuml;', "&uuml;",
        '&rdquo;', '&ldquo;', "&quot;",
        "&nbsp;"];
        
        $change = [" ", " ", 
        "-", 
        "U", 
        ".", 
        "i", 
        "u", "A", "a", 
        "'", "'", "'", 
        "Ö", 'ö', 
        'Ç', "ç",
        'Ü', "ü", 
        '"', '"', '"', 
        " "];

        foreach ($data as $key => $value) {
            /* $item = TEContents::find($value->id);
            
            

            if($item->content) {
                $item->content = str_replace($character, $change, $value->content);
                $item->save();
            } */


            $value->content = str_replace($character, $change, $value->content);
            $value->save();
        }
    }

    public function validateDate($date, $format = 'Y-m-d H:i:s')
    {
        $d = DateTime::createFromFormat($format, $date);
        return $d && $d->format($format) == $date;
    }

    public function insertHaber() {
        $character = ["Ý", "ý", "Þ", "þ", "ð"];
        $change = ["İ", "ı","Ş", "ş", "ğ"];

        foreach (HaberHaberler::where("haberid", "<", 60000)->get() as $item) {

                $title = str_replace($character, $change, $item['haberbaslik']);
                $title_slug = Str::slug($title, '-');
         
                
                
                  TESlugs::insert([
                        [
                            'key' => $title_slug,
                            'reference_type'=> 'TE\Blog\Models\Post',
                            'reference_id' => $item['haberid'],
                            'created_at' => $item['habertarih']." ".$item['habersaat'],
                            'updated_at' => $item['habertarih']." ".$item['habersaat']
                        ]
                    ]);

                    TERedirections::insert([
                        [
                            'from' => '/haber/'.$title_slug.'/'.$item['haberid'],
                            'to' => '/'.$title_slug
                        ]
                    ]); 
                 
                    TEContents::insert([
                        [
                            'reference_type' => 'TE\Blog\Models\Post',
                            'reference_id' => $item['haberid'],
                            'content' => trim(html_entity_decode(str_replace($character, $change, $item['haberhaber']))),
                            'created_at' => $item['habertarih']." ".$item['habersaat'],
                            'updated_at' => $item['habertarih']." ".$item['habersaat']
                        ]
                    ]); 

            
                    TECategorieables::insert([
                        [
                            'category_id' =>  $item['haberkategori'] ? $item['haberkategori'] : 0,
                            'categorieable_type'=> 'TE\Blog\Models\Post', 
                            'categorieable_id'=> $item['haberid'],
                            'created_at' => $item['habertarih']." ".$item['habersaat'],
                            'updated_at' => $item['habertarih']." ".$item['habersaat']
                        ]
                    ]); 

                    TEPosts::insert([
                        [
                            'id'=> $item['haberid'],
                            'name' =>  $title,
                            'author_type'=> 'TE\Users\Models\User', 
                            'author_id'=> 1,
                            'status' => 'published',
                            'created_at' => $item['habertarih']." ".$item['habersaat'],
                            'updated_at' => $item['habertarih']." ".$item['habersaat']
                        ]
                    ]);

                   TEPostProperties::insert([
                        [
                            'post_id' =>  $item['haberid'],
                            'image'=> $item['haberresim'],
                            'description'=> str_replace($character, $change, $item['haberozet']),
                            'position' => '["headline"]', // hem nullable değil hem de default değeri yok
                            'created_at' => $item['habertarih']." ".$item['habersaat'],
                            'updated_at' => $item['habertarih']." ".$item['habersaat']
                        ]
                    ]);

        }
    }

    // MAKALE
    public function insertArticles() {

        $character = ["Ý", "ý", "Þ", "þ", "ð"];
        $change = ["İ", "ı","Ş", "ş", "ğ"];

        foreach (HaberMakale::all() as $item) {
            
            $title = str_replace($character, $change, $item['koseyazisibaslik']);
            $title_slug = Str::slug($title, '-');

           TERedirections::insert([
                [
                    'from' => '/yazar/'.$title_slug.'/'.$item['id'],
                    'to' => '/'.$title_slug
                ]
            ]); 


                TESlugs::insert([
                    [
                        'key' =>  $title_slug,
                        'reference_type'=> 'TE\Authors\Models\Article',
                        'reference_id' => $item['id'],
                        'created_at' => $item['koseyazisitarih']." ".$item['koseyazisisaat'],
                        'updated_at' => $item['koseyazisitarih']." ".$item['koseyazisisaat']
                    ]
                ]);
                
                TEArticles::insert([
                    [
                        'id' => $item['Id'],
                        'author_id'=> $item['koseyazisiyazar'] ? $item['koseyazisiyazar'] : 0,
                        'user_id'=> 1,
                        'name' =>  $title,
                        'description' => "",
                        'status' => 'published',
                        'created_at' => $item['koseyazisitarih']." ".$item['koseyazisisaat'],
                        'updated_at' => $item['koseyazisitarih']." ".$item['koseyazisisaat']

                    ]
                ]);

                TEContents::insert([
                    [
                        'reference_type' => 'TE\Authors\Models\Article',
                        'reference_id' => $item['id'],
                        'content' => trim(html_entity_decode(str_replace($character, $change, $item['koseyazisiyazi']))),
                        'created_at' => $item['koseyazisitarih']." ".$item['koseyazisisaat'],
                        'updated_at' => $item['koseyazisitarih']." ".$item['koseyazisisaat']
                    ]
                ]); 
        }

    }

    // Yorumlar
    public function insertComments(){
        $character = ["Ý", "ý", "Þ", "þ", "ð"];
        $change = ["İ", "ı","Ş", "ş", "ğ"];

        

        foreach (HaberYorumlar::all() as $item) {
            $yorum = str_replace($character, $change, $item['yorumyorum']);
            $yorumad = str_replace($character, $change, $item['yorumad']);
            if($yorumad) {

                TEComments::insert([
                    [
                        'id'=> $item['yorumid'],
                        'name'=> $yorumad,
                        'body' => $yorum,
                        'reference_type' => 'TE\Blog\Models\Post',
                        'reference_id' => $item['yorumhaberid'],
                        'ip' => $item['yorumip'] ? $item['yorumip'] : 0,
                        'status' => $item['yorumonay'],
                        'created_at' => $item['yorumtarih']." ".$item['yorumsaat'],
                        'updated_at' => $item['yorumtarih']." ".$item['yorumsaat']
                    ]
                ]);

                TECommentsLikedIP::insert([
                    [
                        'comment_id'=> $item['yorumid'],
                        'ip' => $item['yorumip'] ? $item['yorumip'] : 0,
                    ]
                ]);

            
            }
        }
    }
}