<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\TECategory;
use App\Models\Ondorthaber\HaberKategori;

use App\Models\TEAuthors;
use App\Models\Ondorthaber\HaberYazarlar;


use App\Models\Ondorthaber\HaberYorumlar;
use App\Models\TEComments;
use App\Models\TECommentsLikedIP;

use App\Models\Ondorthaber\HaberHaberler;
use App\Models\TEPosts; // sadece başlık
use App\Models\TEPostProperties;
use App\Models\TEPostTag;
use App\Models\TECategorieables;
// contents haber içerikleri
// TE\Authors\Models\Article
use App\Models\Ondorthaber\HaberMakale;
use App\Models\TEArticles;
use App\Models\TESlugs;
use App\Models\TEContents;

use App\Models\TERedirections;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

use DateTime;

use App\Models\Ondorthaber\HaberGallery;
use App\Models\Ondorthaber\HaberGalleryItem;

use App\Models\TEMediaFiles;
use App\Models\TEGalleries;
use App\Models\TEGalleryMeta;
use App\Models\TEVideos;

class OndortHaber extends Controller
{
    public $site = '/on4haber-com';


    // galeri tablosundan is_having_video != 1 ise { TE_galleries ve TE_gallery_meta } tablolarına doldur
    // video ise TE_media_files 'a fonksiyon ile doldur ve buranın id'si TE_videos tablosundaki media_id dolsun


    // video galerileri TE_galleries tablosunda
    // galerilere bağlı resimler galeri metada
    // galeri kategori için TE_categories tablosunda bir kayıt olur bu bağlantıda TE_categoriables tablosunda bulunur

    // haberler posta haber desc ve resimleri post properties'e
    // media_files'ta media dosyaları videosta 

    // sanırım galleries tablosunun galerisi oluyor ve categorieable tablosunda reference_id galeries tablosundaki kayıtlar olacak

    public function transferAllDB() {
        
        // $this->insertHaber();
        // $this->insertArticles();
        // $this->insertAuthors();
        // $this->insertComments();
        // $this->insertCategories();
        // $this->updatePostProperties();
        // $this->updateContents();
        // $this->instertGallery();
        $this->insertGalleryMeta();
        //$this->insertAuthors();


        /* foreach (TEVideos::all() as $key => $item) {
            // $item->image = $this->site.'/image/'.$item->image;
            
            TECategorieables::insert([
                [
                    'category_id' => 23,
                    'categorieable_type'=> "TE\Video\Models\Video",
                    'categorieable_id'=> $item->id,
                    'created_at' => $item->created_at,
                    'updated_at' => $item->update_at
                ]
            ]);
            
        } */

        /* foreach (TEGalleries::all() as $key => $item) {
            TECategorieables::insert([
                [
                    'category_id' => 22,
                    'categorieable_type'=> "TE\Gallery\Models\Gallery",
                    'categorieable_id'=> $item->id,
                    'created_at' => $item->created_at,
                    'updated_at' => $item->update_at
                ]
            ]);
            
        } */

        /* foreach (TEGalleries::all() as $key => $item) {
            $item->image = $this->site.'/image/'.$item->image;
            $item->save();
        } */

        /* foreach (TESlugs::all() as $key => $item) {
            if($item->id > 17957 && $item->id < 18019) {
                $item->prefix = 'plugins.gallery::slug.galleries';
                $item->save();
            }

            if($item->id > 18019) {
                $item->prefix = 'plugins.video::slug.video';
                $item->save();
            }
        } *&

        
        // galeri - video - post - makale - category herşeyin slug tablosuna aktarılması lazım
        // İLGİLİ METODLARIN İÇİNE SLUG INSERTLERİ EKLE
        /* foreach (TEGalleries::all() as $key => $item) {

            TESlugs::insert([
                [
                    'key' => Str::slug($item['name'], '-'),
                    'reference_type'=> "TE\Gallery\Models\Gallery",
                    'reference_id'=> $item->id,
                    'created_at' => $item->created_at,
                    'updated_at' => $item->update_at
                ]
            ]);

        }

        foreach (TEVideos::all() as $key => $item) {

            TESlugs::insert([
                [
                    'key' => Str::slug($item['name'], '-'),
                    'reference_type'=> "TE\Video\Models\Video",
                    'reference_id'=> $item->id,
                    'created_at' => $item->created_at,
                    'updated_at' => $item->update_at
                ]
            ]);
        } */
    }

    /*
        galeriyi aldık galery metaları aldık - metalardaki image path site/img path sıkıntı var mı?
        videoları sadece TEMediaFiles tablosuna alıp bırakıyor muyuz başka bir tablo ile iş var mı
        videolarda hem normal video dosyası pathi var hem yt var ikisi birden olan da var
    */

    public function insertGalleryMeta() {

        foreach (TEGalleries::all() as $key => $item) {

            $gallery_meta = [];

            foreach (HaberGalleryItem::where('galery_id', $item->id)->get() as $k => $gallery_item) {
                
                $meta_data = [
                    "img" => $this->site."/image/".$gallery_item->image_name,
                    "description" => trim($gallery_item->description)
                ];

                array_push($gallery_meta, $meta_data);

            }

            if(count($gallery_meta) > 0) {
            
                TEGalleryMeta::insert([
                    [
                        'images' => json_encode($gallery_meta),
                        'reference_type'=> "TE\Gallery\Models\Gallery",
                        'reference_id'=> $item->id,
                        'created_at' => $this->validateDate(explode(".", $item->created_at)[0]) ? explode(".", $item->created_at)[0] : "",
                        'updated_at' => $this->validateDate(explode(".", $item->updated_at)[0]) ? explode(".", $item->updated_at)[0] : ""
                    ]
                ]);

            }

        }

    }

    public function instertGallery() {
        $character = ["&not;", "&LTR;", "&lrm;", 
        "&ndash;", 
        "&Ucirc;", 
        "&hellip;", 
        "&icirc;", 
        "&ucirc;", "&Acirc;", "&acirc;",
        "&lsquo;", "&#39;", "&rsquo;", 
        "&Ouml;", '&ouml;', 
        '&Ccedil;', "&ccedil;",
        '&Uuml;', "&uuml;",
        '&rdquo;', '&ldquo;', "&quot;",
        "&nbsp;",
        "u0131", "u003cpu003e", "u003c/pu003e","\u00f6"];
        
        $change = [" ", " ", 
        "-", 
        "U", 
        ".", 
        "i", 
        "u", "A", "a", 
        "'", "'", "'", 
        "Ö", 'ö', 
        'Ç', "ç",
        'Ü', "ü", 
        '"', '"', '"', 
        " ",
        "ı", "<p>", "</p>", "ö"];
        
        foreach (HaberGallery::all() as $key => $item) {

         
            
            if($item->is_have_video) {
               
                // youtube $url
                if($item->video_link) {
                    $is_v = explode("?v=", $item->video_link);

                    if(count($is_v) > 1) {
                        $url = $item->video_link;
                    } else {
                        $url = 'https://www.youtube.com/watch?v=' . substr(explode("embed", $item->video_link)[1], 1);
                    }


                    $mime_type = 'youtube';
                }


                if($item->video_name) {
                    $mime_type = 'video/mp4';
                    $url = $item->video_name;
                }

                /* TEMediaFiles::insert([
                    [
                        'id' => $item->id,
                        'user_id' => 1,
                        'name'=> $item->title,
                        'folder_id' => 0,
                        'options' => "",
                        'size' => 0,
                        'mime_type' => $mime_type,
                        'url' => $url,
                        'created_at' => $this->validateDate(explode(".", $item->created_at)[0]) ? explode(".", $item->created_at)[0] : "",
                        'updated_at' => $this->validateDate(explode(".", $item->updated_at)[0]) ? explode(".", $item->updated_at)[0] : ""
                    ]
                ]); */

                TEVideos::insert([
                    [
                        'name'=> $item->title,
                        'description'=> $item->title,
                        'is_featured'=> 0,
                        'order'=> $item->order_number ? $item->order_number : 1,
                        'headline'=> 0,
                        'story'=> 0,
                        'image'=> $item->preview_image,
                        'hide_headline'=> 0,
                        'media_id'=> $item->id,
                        'user_id' => 1,
                        'status' => 'published',
                        'created_at' => $this->validateDate(explode(".", $item->created_at)[0]) ? explode(".", $item->created_at)[0] : "",
                        'updated_at' => $this->validateDate(explode(".", $item->updated_at)[0]) ? explode(".", $item->updated_at)[0] : ""
                    ]
                ]);

            } else {

                TERedirections::insert([
                    [
                        'from' => '/galeri/'.$item['id'].'/'.Str::slug($item['title'], '-'),
                        'to' => '/'.Str::slug($item['title'], '-'),
                        'status' => 'published'
                    ]
                ]); 

                /* TEGalleries::insert([
                    [
                        'id' => $item['id'],
                        'name'=> $item['title'],
                        'description' => str_replace($character, $change, $item['spot']),
                        'order' => $item['order_number'],
                        'image' => $item['preview_image'],
                        'user_id' => 0,
                        'status' => "published",
                        'created_at' => $this->validateDate(explode(".", $item->created_at)[0]) ? explode(".", $item->created_at)[0] : "",
                        'updated_at' => $this->validateDate(explode(".", $item->updated_at)[0]) ? explode(".", $item->updated_at)[0] : ""
                    ]
                ]); */

            }

        }
        
    }

    public function updateContents() {
        $data = TEContents::all();
        
        $character = ["&not;", "&LTR;", "&lrm;", 
        "&ndash;", 
        "&Ucirc;", 
        "&hellip;", 
        "&icirc;", 
        "&ucirc;", "&Acirc;", "&acirc;",
        "&lsquo;", "&#39;", "&rsquo;", 
        "&Ouml;", '&ouml;', 
        '&Ccedil;', "&ccedil;",
        '&Uuml;', "&uuml;",
        '&rdquo;', '&ldquo;', "&quot;",
        "&nbsp;"];
        
        $change = [" ", " ", 
        "-", 
        "U", 
        ".", 
        "i", 
        "u", "A", "a", 
        "'", "'", "'", 
        "Ö", 'ö', 
        'Ç', "ç",
        'Ü', "ü", 
        '"', '"', '"', 
        " "];

        foreach ($data as $key => $value) {
            $item = TEContents::find($value->id);
            
            

            if($item->content) {
                // $item->content = str_replace("&lsquo;", "'", str_replace("&#39;", "'", str_replace("&rsquo;", "'", str_replace('&ouml;', 'ö', str_replace('&Ccedil;', 'Ç', str_replace('&Uuml;', 'Ü', str_replace('&rdquo;', '"', str_replace('&ldquo;', '"', str_replace("&ccedil;", "ç", str_replace("&uuml;", "ü", $value->content))))))))));
                $item->content = str_replace($character, $change, $value->content);
                $item->save();
            }
        }
    }

    public function updatePostProperties() {
        $data = TEPostProperties::all();

        foreach ($data as $key => $value) {
            $item = TEPostProperties::find($value->id);

            if($item->image) {
                $item->image = $this->site."/image/".substr($value->image, 14);
                $item->save();
            }
        }
    }

    public function validateDate($date, $format = 'Y-m-d H:i:s')
    {
        $d = DateTime::createFromFormat($format, $date);
        return $d && $d->format($format) == $date;
    }

    public function insertHaber() {

        foreach (HaberHaberler::all() as $item) {

            $category = HaberKategori::find($item['category_id']);

            // $is_slug = TESlugs::where('reference_id', $item['id'])->count();
       
             if($item['category_id'] && $category && isset($category->url)) {
                
                
                   TESlugs::insert([
                        [
                            'key' => Str::slug($item['title'], '-'),
                            'reference_type'=> 'TE\Blog\Models\Post',
                            'reference_id' => $item['id'],
                            'created_at' => $this->validateDate(explode(".", $item->created_at)[0]) ? explode(".", $item->created_at)[0] : "",
                            'updated_at' => $this->validateDate(explode(".", $item->updated_at)[0]) ? explode(".", $item->updated_at)[0] : ""
                        ]
                    ]);

                    TERedirections::insert([
                        [
                            'from' => '/'.$category->url.'/haber/'.$item['id'].'/'.Str::slug($item['title'], '-'),
                            'to' => '/'.Str::slug($item['title'], '-')
                        ]
                    ]); 
                 
                    TEContents::insert([
                        [
                            'reference_type' => 'TE\Blog\Models\Post',
                            'reference_id' => $item['id'],
                            'content' => trim(html_entity_decode($item['new_content'])),
                            'created_at' => $this->validateDate(explode(".", $item->created_at)[0]) ? explode(".", $item->created_at)[0] : "",
                            'updated_at' => $this->validateDate(explode(".", $item->updated_at)[0]) ? explode(".", $item->updated_at)[0] : ""
                        ]
                    ]); 

            
                    TECategorieables::insert([
                        [
                            'category_id' =>  $item['category_id'] ? $item['category_id'] : 0,
                            'categorieable_type'=> 'TE\Blog\Models\Post', 
                            'categorieable_id'=> $item['id'],
                        ]
                    ]);

                    TEPosts::insert([
                        [
                            'id'=> $item['id'],
                            'name' =>  $item['title'],
                            'author_type'=> 'TE\Users\Models\User', 
                            'author_id'=> 1,
                            'status' => 'published',
                            'created_at' => $this->validateDate(explode(".", $item->created_at)[0]) ? explode(".", $item->created_at)[0] : "",
                            'updated_at' => $this->validateDate(explode(".", $item->updated_at)[0]) ? explode(".", $item->updated_at)[0] : ""
                        ]
                    ]);

                    TEPostProperties::insert([
                        [
                            'post_id' =>  $item['id'],
                            'image'=> $item['news_image'],
                            'description'=> $item['spot'],
                            'position' => '["headline"]', // hem nullable değil hem de default değeri yok
                            'created_at' => $this->validateDate(explode(".", $item->created_at)[0]) ? explode(".", $item->created_at)[0] : "",
                            'updated_at' => $this->validateDate(explode(".", $item->updated_at)[0]) ? explode(".", $item->updated_at)[0] : ""
                        ]
                    ]);

            }

        }
    }

    // MAKALE
    public function insertArticles() {

        foreach (HaberMakale::all() as $item) {
            
           TERedirections::insert([
                [
                    'from' => '/koseyazisi/'.$item['Id']."/".Str::slug($item['Title'], '-'),
                    'to' => '/'.Str::slug($item['Title'], '-')
                ]
            ]); 


                TESlugs::insert([
                    [
                        'key' =>  Str::slug($item['Title']),
                        'reference_type'=> 'TE\Authors\Models\Article',
                        'reference_id' => $item['Id'],
                        'created_at' => $item->created_at ? explode(".", $item->created_at)[0]: "",
                        'updated_at' => $item->updated_at ? explode(".", $item->updated_at)[0]: "",
                    ]
                ]);
                
                TEArticles::insert([
                    [
                        'id' => $item['Id'],
                        'author_id'=> $item['AutherId'] ? $item['AutherId'] : 0,
                        'user_id'=> $item['AutherId'] ? $item['AutherId'] : 0,
                        'name' =>  $item['Title'],
                        'description' => "",
                        'status' => 'published',
                        'created_at' => $item->created_at ? explode(".", $item->created_at)[0]: "",
                        'updated_at' => $item->updated_at ? explode(".", $item->updated_at)[0]: "",

                    ]
                ]);

                TEContents::insert([
                    [
                        'reference_type' => 'TE\Authors\Models\Article',
                        'reference_id' => $item['Id'],
                        'content' => trim(html_entity_decode($item['ArticleContent'])),
                        'created_at' => $item->created_at ? explode(".", $item->created_at)[0]: "",
                        'updated_at' => $item->updated_at ? explode(".", $item->updated_at)[0]: "",
                    ]
                ]); 

            


        }

    }

    // Yazarlar
    public function insertAuthors(){
     
        foreach (HaberYazarlar::all() as $item) {

            if($item['first_name']) {
                /* TESlugs::insert([
                    [
                        'key' =>  Str::slug($item['first_name']." ".$item['last_name']),
                        'reference_type'=> 'TE\Authors\Models\Author',
                        'reference_id' => $item['id'] > 0 ? $item['id'] : 0,
                        'created_at' => explode(".", $item->created_at)[0],
                        'updated_at' => explode(".", $item->updated_at)[0],
                    ]
                ]); */

                TEAuthors::insert([
                    [
                        'id'=> $item['id'],
                        'name' => $item['first_name']." ".$item['last_name'],
                        'image' => '/'.$this->site.'/image/'.$item['image_name'],
                        'status' => $item['is_active'],
                        'order' => $item['order_number'],
                        'created_at' => explode(".", $item->created_at)[0],
                        'updated_at' => explode(".", $item->updated_at)[0],
                    ]
                ]);
                
            }

        }
    }

    // Yorumlar
    public function insertComments(){
     
        foreach (HaberYorumlar::all() as $item) {
            

                TEComments::insert([
                    [
                        'id'=> $item['id'],
                        'name'=> $item['full_name'],
                        'body' => $item['new_comment'],
                        'reference_type' => 'TE\Blog\Models\Post',
                        'reference_id' => $item['new_id'],
                        'ip' => $item['ip'] ? $item['ip'] : 0,
                        'status' => $item['is_allow'],
                        'created_at' => explode(".", $item->created_at)[0],
                        'updated_at' => explode(".", $item->updated_at)[0],
                    ]
                ]);

                TECommentsLikedIP::insert([
                    [
                        'comment_id'=> $item['id'],
                        'ip' => $item['ip'] ? $item['ip'] : 0,
                    ]
                ]);

            
        }
    }

    // Kategoriler
    public function insertCategories() {
      
        foreach (HaberKategori::all() as $item) {

           

                TERedirections::insert([
                    [
                        'from' => '/haberler/'.$item['url'],
                        'to' => '/'.$item['url']
                    ]
                ]); 
    
                
                TECategory::insert([
                    [
                        'id'=> $item['id'],
                        'name'=> $item['name'],
                        'reference' => 'TE\Blog\Models\Post',
                        'status' => $item['is_active'],
                        'order' => $item['order_number'],
                        'author_type' => '',
                        'description' => $item['name'],
                        'created_at' => explode(".", $item->created_at)[0],
                        'updated_at' => explode(".", $item->updated_at)[0]
                    ]
                ]);
    
                TESlugs::insert([
                    [
                        'key' =>  $item['url'],
                        'reference_type'=> 'TE\Category\Models\Category',
                        'reference_id' => $item['id'],
                        'created_at' => date('Y-m-d H:i:s'),
                        'updated_at' => date('Y-m-d H:i:s')
                    ]
                ]);

        

        }
    }
}