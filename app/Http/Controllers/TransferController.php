<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\TECategory;
use App\Models\HaberKategori;

use App\Models\TEAuthors;
use App\Models\HaberYazarlar;


use App\Models\HaberYorumlar;
use App\Models\TEComments;
use App\Models\TECommentsLikedIP;

use App\Models\HaberHaberler;
use App\Models\TEPosts; // sadece başlık
use App\Models\TEPostProperties;
use App\Models\TEPostTag;
use App\Models\TECategorieables;
// contents haber içerikleri
// TE\Authors\Models\Article
use App\Models\HaberMakale;
use App\Models\TEArticles;
use App\Models\TESlugs;
use App\Models\TEContents;

use App\Models\TERedirections;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

/* 59 HABER - kurumsalx aktarım*/


// kategorilerde keywordler var virgüllü onlar slug olarak eklenbilir

class TransferController extends Controller
{
    public $site = '/59haber-com';
    public function transferAllDB() {
        // $this->insertCategories();
        
        // $this->insertHaber();
        // $this->insertArticles();
        
        // $this->insertAuthors();
        // $this->insertComments();
        
        // $this->updateContents();
        
        // $this->updatePostProperties();    
    }

    public function haberEditForCharacter() {

        //$data = TEContents::where('reference_id', 20314)->get();
        /*$data = TEContents::where('reference_id', 19416)->get();
        echo $data[0]->content;

        return; 
        */
        foreach (HaberHaberler::all() as $item) {
       
             if($item['HaberKategoriId']) {

                if($item['tarih'] != '0000-00-00 00:00:00') {

                        TEContents::insert([
                            [
                                'reference_type' => 'TE\Blog\Models\Post',
                                'reference_id' => $item['id'],
                                'content' => strip_tags(trim(html_entity_decode($item['Icerik']))),
                                //'content' => html_entity_decode($item['Icerik']),
                                'created_at' => $item['tarih'],
                                'updated_at' => $item['tarih']
                            ]
                        ]); 

                }

            }
          

        }
    }

    public function updateContents() {
        $data = TEContents::all();
        
        $character = ["&not;", "&LTR;", "&lrm;", 
        "&ndash;", 
        "&Ucirc;", 
        "&hellip;", 
        "&icirc;", 
        "&ucirc;", "&Acirc;", "&acirc;",
        "&lsquo;", "&#39;", "&rsquo;", 
        "&Ouml;", '&ouml;', 
        '&Ccedil;', "&ccedil;",
        '&Uuml;', "&uuml;",
        '&rdquo;', '&ldquo;', "&quot;",
        "&nbsp;"];
        
        $change = [" ", " ", 
        "-", 
        "U", 
        ".", 
        "i", 
        "u", "A", "a", 
        "'", "'", "'", 
        "Ö", 'ö', 
        'Ç', "ç",
        'Ü', "ü", 
        '"', '"', '"', 
        " "];

        foreach ($data as $key => $value) {
            $item = TEContents::find($value->id);
            
            

            if($item->content) {
                // $item->content = str_replace("&lsquo;", "'", str_replace("&#39;", "'", str_replace("&rsquo;", "'", str_replace('&ouml;', 'ö', str_replace('&Ccedil;', 'Ç', str_replace('&Uuml;', 'Ü', str_replace('&rdquo;', '"', str_replace('&ldquo;', '"', str_replace("&ccedil;", "ç", str_replace("&uuml;", "ü", $value->content))))))))));
                $item->content = str_replace($character, $change, $value->content);
                $item->save();
            }
        }
    }

    public function updatePostProperties() {
        $data = TEPostProperties::all();

        foreach ($data as $key => $value) {
            $item = TEPostProperties::find($value->id);

            if($item->image) {
                $item->image = $this->site.$value->image;
                $item->save();
            }
        }
    }

    public function insertHaber() {

        foreach (HaberHaberler::all() as $item) {
       
             if($item['HaberKategoriId']) {

                $slug = explode("_", trim($item['Sef']));

                $trimmedArray = array_map('trim', $slug);
                $emptyRemoved = array_filter($trimmedArray);
                
                $slug2 = implode("-", $emptyRemoved);


                if($item['tarih'] != '0000-00-00 00:00:00') {
                        
                    TESlugs::insert([
                        [
                            'key' =>  $slug2,
                            'reference_type'=> 'TE\Blog\Models\Post',
                            'reference_id' => $item['id'],
                            'created_at' => $item['tarih']
                        ]
                    ]);


                    // substr("abcdef", 0, -1)
                    TERedirections::insert([
                        [
                            'from' => '/haber/'.trim($item['Sef']).'-'.$item['id'].'.html',
                            'to' => '/'.$slug2,
                            'status' => 'published'
                        ]
                    ]); 
                 
                    TEContents::insert([
                        [
                            'reference_type' => 'TE\Blog\Models\Post',
                            'reference_id' => $item['id'],
                            'content' => str_replace("&ccedil;", "ç", str_replace("&uuml;", "ü", strip_tags(trim(html_entity_decode($item['Icerik']))))),
                            'created_at' => $item['tarih'],
                            'updated_at' => $item['tarih']
                        ]
                    ]); 

            
                    TECategorieables::insert([
                        [
                            'category_id' =>  $item['HaberKategoriId'] ? $item['HaberKategoriId'] : 0,
                            'categorieable_type'=> 'TE\Blog\Models\Post', 
                            'categorieable_id'=> $item['id'],
                        ]
                    ]);
            
                

                    TEPosts::insert([
                        [
                            'id'=> $item['id'],
                            'name' =>  $item['baslik'],
                            'author_type'=> 'TE\Users\Models\User', 
                            'author_id'=> 1, // haber tablosunda author_id yok
                            'status' => 'published',
                            'created_at' => $item['tarih'],
                            'updated_at' => $item['tarih']
                        ]
                    ]);

                    TEPostProperties::insert([
                        [
                            'post_id' =>  $item['id'],
                            'image'=> $item['resim1'],
                            'position' => '["headline"]', // hem nullable değil hem de default değeri yok
                            'created_at' => $item['tarih'],
                            'updated_at' => $item['tarih']
                        ]
                    ]);
                }

            } else {
                    

                        // category id yok yani haber değil page

            } 
          

        }
    }

    // MAKALE
    public function insertArticles() {

        foreach (HaberMakale::all() as $item) {
            

            if($item['id'] > 0 && $item['KullaniciId'] > 0) {

               

                TERedirections::insert([
                    [
                        'from' => '/kose-yazilari/'.Str::slug($item['baslik'], '_').'-'.$item['id'].'.html',
                        'to' => '/'.Str::slug($item['baslik'], '-'),
                        'status' => 'published'
                    ]
                ]); 

                
                TESlugs::insert([
                    [
                        'key' =>  Str::slug($item['baslik']),
                        'reference_type'=> 'TE\Authors\Models\Article',
                        'reference_id' => $item['id'],
                        'created_at' => date('Y-m-d H:i:s'),
                        'updated_at' => date('Y-m-d H:i:s')
                    ]
                ]);
                
                
                
                TEArticles::insert([
                    [
                        'id' => $item['id'],
                        'author_id'=> $item['KullaniciId'] ? $item['KullaniciId'] : 0,
                        'user_id'=> $item['KullaniciId'] ? $item['KullaniciId'] : 0,
                        'name' =>  $item['baslik'],
                        'description' => $item['spot'],
                        'status' => 'published',
                        'created_at' => date('Y-m-d H:i:s'),
                        'updated_at' => date('Y-m-d H:i:s')

                    ]
                ]);

                TEContents::insert([
                    [
                        'reference_type' => 'TE\Authors\Models\Article',
                        'reference_id' => $item['id'],
                        'content' => str_replace("&ccedil;", "ç", str_replace("&uuml;", "ü", strip_tags(trim(html_entity_decode($item['Icerik']))))),
                        'created_at' => date('Y-m-d H:i:s'),
                        'updated_at' => date('Y-m-d H:i:s')
                    ]
                ]); 
                
            }


        }

    }

    // Yazarlar
    public function insertAuthors(){
     
        foreach (HaberYazarlar::all() as $item) {

            if($item['id'] > 0) {

                if($item['adi']) {
                    TESlugs::insert([
                        [
                            'key' =>  Str::slug($item['adi']),
                            'reference_type'=> 'TE\Authors\Models\Author',
                            'reference_id' => $item['id'] > 0 ? $item['id'] : 0,
                            'created_at' => date('Y-m-d H:i:s'),
                            'updated_at' => date('Y-m-d H:i:s')
                        ]
                    ]);
                }
    
                
                if($item['id'] > 0) {
                    TEAuthors::insert([
                        ['id'=> $item['id'], 'name' => $item['adi'], 'status' => $item['aktif']]
                    ]);
                } else if ($item['id'] == -1) {
                    TEAuthors::insert([
                        ['id'=> 1, 'name' => $item['adi'], 'status' => $item['aktif']]
                    ]);
                } else if($item['id'] == -2) {
                    TEAuthors::insert([
                        ['id'=> 2, 'name' => $item['adi'], 'status' => $item['aktif']]
                    ]);
                }

            }
           
        }
    }

    // Yorumlar
    public function insertComments(){
     
        foreach (HaberYorumlar::all() as $item) {
            

            if($item['Tip'] == 1) { // HABER

                TEComments::insert([
                    [
                        'id'=> $item['Id'],
                        'name'=> $item['Isim'],
                        'body' => $item['Mesaj'],
                        'reference_type' => 'TE\Blog\Models\Post',
                        'reference_id' => $item['TipId'],
                        'ip' => $item['Ip'] ? $item['Ip'] : 0,
                        'status' => $item['Onay'],
                        'created_at' => $item['Tarih'],
                        'updated_at' => $item['Tarih']
                    ]
                ]);

                TECommentsLikedIP::insert([
                    [
                        'comment_id'=> $item['Id'],
                        'ip' => $item['Ip'] ? $item['Ip'] : 0,
                    ]
                ]);

            }
            
            if($item['Tip'] == 2) { // MAKALE

                TEComments::insert([
                    [
                        'id'=> $item['Id'],
                        'name'=> $item['Isim'],
                        'body' => $item['Mesaj'],
                        'reference_type' => 'TE\Authors\Models\Article',
                        'reference_id' => $item['TipId'],
                        'ip' => $item['Ip'] ? $item['Ip'] : 0,
                        'status' => $item['Onay'],
                        'created_at' => $item['Tarih'],
                        'updated_at' => $item['Tarih']
                    ]
                ]);

                TECommentsLikedIP::insert([
                    [
                        'comment_id'=> $item['Id'],
                        'ip' => $item['Ip'] ? $item['Ip'] : 0,
                    ]
                ]);

            }
            
        }
    }

    // Kategoriler
    public function insertCategories() {
        foreach (HaberKategori::all() as $item) {

            if($item['Id'] > 0) {

                TERedirections::insert([
                    [
                        'from' => '/haber/kategori/'.$item['Sef'].'-'.$item['Id'].'.html',
                        'to' => '/'.$item['Sef'],
                        'status' => 'published'
                    ]
                ]); 
    
                
                TECategory::insert([
                    [
                        'id'=> $item['Id'],
                        'name'=> $item['Adi'],
                        'reference' => 'TE\Blog\Models\Post',
                        'status' => $item['Onay'],
                        'order' => $item['Sira'],
                        'author_type' => '',
                        'description' => $item['Descriptions'],
                        'created_at' => date('Y-m-d H:i:s'),
                        'updated_at' => date('Y-m-d H:i:s')
                    ]
                ]);
    
                TESlugs::insert([
                    [
                        'key' =>  $item['Sef'],
                        'reference_type'=> 'TE\Category\Models\Category',
                        'reference_id' => $item['Id'],
                        'created_at' => date('Y-m-d H:i:s'),
                        'updated_at' => date('Y-m-d H:i:s')
                    ]
                ]);
                

            }

        }
    }
}