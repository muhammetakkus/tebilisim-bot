<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Turkgun\Posts;
use App\Models\TEGalleryMeta;
use DateTime;


class Turkgun extends Controller
{
    public $site = '/turkgun-com';


    // galeri tablosundan is_having_video != 1 ise { TE_galleries ve TE_gallery_meta } tablolarına doldur
    // video ise TE_media_files 'a fonksiyon ile doldur ve buranın id'si TE_videos tablosundaki media_id dolsun


    // video galerileri TE_galleries tablosunda
    // galerilere bağlı resimler galeri metada
    // galeri kategori için TE_categories tablosunda bir kayıt olur bu bağlantıda TE_categoriables tablosunda bulunur

    // haberler posta haber desc ve resimleri post properties'e
    // media_files'ta media dosyaları videosta 

    // sanırım galleries tablosunun galerisi oluyor ve categorieable tablosunda reference_id galeries tablosundaki kayıtlar olacak

    public function transferAllDB() {
        
        // $this->insertHaber();
        // $this->insertArticles();
        // $this->insertAuthors();
        // $this->insertComments();
        // $this->insertCategories();
        // $this->updatePostProperties();
        // $this->updateContents();
        // $this->instertGallery();
        // $this->insertGalleryMeta();
        //$this->insertAuthors();

        // $post = new Posts();
        // $p = $post->skip(0)->take(10)->get();
        // substr("abcdef", 2, -1)
        // $aa = new TEGalleryMeta();
       
        

        $character = ["&not;", "&LTR;", "&lrm;", 
        "&ndash;", 
        "&Ucirc;", 
        "&hellip;", 
        "&icirc;", 
        "&ucirc;", "&Acirc;", "&acirc;",
        "&lsquo;", "&#39;", "&rsquo;", 
        "&Ouml;", '&ouml;', 
        '&Ccedil;', "&ccedil;",
        '&Uuml;', "&uuml;",
        '&rdquo;', '&ldquo;', "&quot;",
        "&nbsp;",
        "u003cbru003e",  "u003cpu003e", "u003c/pu003e","\\\u00f6", "u003cbru003eö</p>ö</p>ö", "u003cbru003eö</p>ö"];
        
        $change = [" ", " ", 
        "-", 
        "U", 
        ".", 
        "i", 
        "u", "A", "a", 
        "'", "'", "'", 
        "Ö", 'ö', 
        'Ç', "ç",
        'Ü', "ü", 
        '"', '"', '"', 
        " ",
        "",  "<p>", "</p>", "", "",""];
    
        
    
        /* foreach (json_decode($aa->first()->images) as $key => $value ) {
            echo str_replace($character, $change, trim($value->description));
        } */

        // $kk = TEGalleryMeta::where('reference_id', 194846)->first();
        // dd(json_decode($kk->images));
            

        // $p = Posts::where('gallery', '!=', '')->where('integer_id', 194846)->get();
        $p = Posts::where('gallery', '!=', '')->get();
      
        foreach ($p as $key => $item) {
            
            $gallery = TEGalleryMeta::where('reference_id', $item->integer_id)->first();

            if(!empty($gallery)) {

          
                $gallery_meta = [];

                if(gettype($item->gallery) === 'string') {
                    $gallerydata = json_decode($item->gallery);


                    if(is_array($gallerydata)) {

                        if(count($gallerydata) > 0) {

                            foreach($gallerydata as $g) {

                                // dd(strip_tags(str_replace($character, $change, trim($g->summary))));

                                $meta_data = [
                                    "img" => $this->site.$g->image,
                                    "description" => strip_tags(str_replace($character, $change, trim($g->summary)))
                                ];
            
                                array_push($gallery_meta, $meta_data);
                            }
            
                            
                            if(count($gallery_meta) > 0) {
                                
                                $gallery->images = json_encode($gallery_meta);
                                $gallery->save();
            
                            }

                        }

                    } 

                }
                
            }
            
        }

    }

}