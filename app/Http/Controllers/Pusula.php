<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\TECategory;
use App\Models\TEAuthors;
use App\Models\TEComments;
use App\Models\TECommentsLikedIP;

use App\Models\TEPosts; // sadece başlık
use App\Models\TEPostProperties;
use App\Models\TEPostTag;
use App\Models\TECategorieables;
// contents haber içerikleri
// TE\Authors\Models\Article
use App\Models\TEArticles;
use App\Models\TESlugs;
use App\Models\TEContents;
use App\Models\TERedirections;
use App\Models\TEMediaFiles;
use App\Models\TEGalleries;
use App\Models\TEGalleryMeta;
use App\Models\TEVideos;
use DateTime;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

use App\Models\Pusula\HaberHaberler;
use App\Models\Pusula\HaberMakale;
use App\Models\Pusula\HaberGallery;
use App\Models\Pusula\HaberGalleryItem;
use App\Models\Pusula\HaberYazarlar;
use App\Models\Pusula\HaberYorumlar;
use App\Models\Pusula\HaberKategori;
use App\Models\Pusula\Video;

/* - Haberler table -> xhnsx_haberler 
- okunma sayılarını -> xhnsx_haberlog
- Arşiv haberler -> xhnsx_arhaberler 
- arşiv okunma sayıları -> xhnsx_arhaberlog
- Haber kategorileri :zhnsx_hkategori
- arşiv haber kategori :xhnsx_arhkategori
- Haber yorumları :xhnsx_hyorum
- Arşiv haber yorum :xhnsx_arhyorum
- xhnsx_fotgal_bas
- xhnsx_fotresim
- xhnsx_fotvideo
*/

class Pusula extends Controller
{
    public $site = '/pusula-com';

    public function transferAllDB() {
        // $this->insertCategories();
        // $this->insertAuthors();
        // $this->insertArticles();

        $this->insertHaber();
        // $this->insertComments();

        // $this->instertGallery();
        // $this->insertGalleryMeta();
        // $this->instertVideo();
        //$this->updatePostProperties();
        // $this->updateContents();


   
  
        // video için prefix - normalde videolar eklenirken eklenmeli
        /* foreach (TESlugs::where('id', '>', 140484)->get() as $key => $item) {

                $item->prefix = 'plugins.video::slug.video';
                $item->save();
            
        } */

        
        
    }

    // Yazarlar
    public function insertAuthors(){

        $character = ["Ý", "ý", "Þ", "þ", "ð"];
        $change = ["İ", "ı","Ş", "ş", "ğ"];
    
        foreach (HaberYazarlar::all() as $item) {

            if($item['uye_adi']) {
                
                $yazar_name = $item['uye_adi'].' '.$item['uye_soyad'];

                TESlugs::insert([
                    [
                        'key' =>  Str::slug($yazar_name),
                        'reference_type'=> 'TE\Authors\Models\Author',
                        'reference_id' => $item['id'] > 0 ? $item['id'] : 0,
                        'created_at' => date('Y-m-d H:i:s'),
                        'updated_at' => date('Y-m-d H:i:s')
                    ]
                ]);

                TEAuthors::insert([
                    [
                        'id'=> $item['id'],
                        'name' => $yazar_name,
                        'email' => $item['uye_email'],
                        'image' => $item['resim'] ? $this->site.'/'.$item['resim'] : '',
                        'status' => $item['aktif'],
                        'order' => 1,
                        'created_at' => date('Y-m-d H:i:s'),
                        'updated_at' => date('Y-m-d H:i:s')
                    ]
                ]);
                
            }

        }
    }

    // Kategoriler
    public function insertCategories() {

        $character = ["Ý", "ý", "Þ", "þ", "ð"];
        $change = ["İ", "ı","Ş", "ş", "ğ"];
      
        foreach (HaberKategori::all() as $item) {
            $category = $item['kategori'];

            if($item['katid']) {

                TERedirections::insert([
                    [
                        'from' => '/'.Str::slug($category),
                        'to' => '/'.Str::slug($category),
                        'status' => 'published'
                    ]
                ]);
    
              TECategory::insert([
                    [
                        'id'=> $item['katid'],
                        'name'=> $category,
                        'image'=> $item['kresim'] ? $this->site.'/image/'.$item['kresim'] : '',
                        'reference' => 'TE\Blog\Models\Post',
                        'order' => $item['sira'],
                        'author_type' => '',
                        'status' => 1,
                        'created_at' => date('Y-m-d H:i:s'),
                        'updated_at' => date('Y-m-d H:i:s')
                    ]
                ]);
    
                TESlugs::insert([
                    [
                        'key' =>  Str::slug($category),
                        'reference_type'=> 'TE\Category\Models\Category',
                        'reference_id' => $item['katid'],
                        'created_at' => date('Y-m-d H:i:s'),
                        'updated_at' => date('Y-m-d H:i:s')
                    ]
                ]);

            }

        }
    }


    public function insertGalleryMeta() {

        foreach (TEGalleries::all() as $key => $item) {

            $gallery_meta = [];

            foreach (HaberGalleryItem::where('galerikatid', $item->id)->get() as $k => $gallery_item) {
                
                $meta_data = [
                    "img" => $this->site."/image/".$gallery_item->galeriphoto,
                    "description" => trim($gallery_item->galeritext)
                ];

                array_push($gallery_meta, $meta_data);

            }

            if(count($gallery_meta) > 0) {
            
                TEGalleryMeta::insert([
                    [
                        'images' => json_encode($gallery_meta),
                        'reference_type'=> "TE\Gallery\Models\Gallery",
                        'reference_id'=> $item->id,
                        'created_at' => date('Y-m-d H:i:s'),
                        'updated_at' => date('Y-m-d H:i:s')
                    ]
                ]);

            }

        }

    }

    public function instertGallery() {
        $character = ["Ý", "ý", "Þ", "þ", "ð"];
        $change = ["İ", "ı","Ş", "ş", "ğ"];
        
     

        foreach (HaberGallery::all() as $key => $item) {

            $galery_title = str_replace($character, $change, $item['galeribaslik']);

          /*  TERedirections::insert([
                [
                    'from' => '/galeri/'.Str::slug($galery_title, '-').'/'.$item['galeriid'],
                    'to' => '/'.Str::slug($galery_title, '-'),
                    'status' => 'published'
                ]
            ]); 

            TEGalleries::insert([
                [
                    'id' => $item['galeriid'],
                    'name'=> $galery_title,
                    'description' => '',
                    'order' => 1,
                    'image' => $item['galeriresim'],
                    'user_id' => 0,
                    'status' => "published",
                    'created_at' => $item['galeritarih'],
                    'updated_at' => date('Y-m-d H:i:s')
                ]
            ]);

            TECategorieables::insert([
                [
                    'category_id' => 18,
                    'categorieable_type'=> "TE\Gallery\Models\Gallery",
                    'categorieable_id'=> $item['galeriid'],
                    'created_at' => $item['galeritarih'],
                    'updated_at' => date('Y-m-d H:i:s')
                ]
            ]); */

            /* if($item->id > 17957 && $item->id < 18019) {
                $item->
                $item->save();
            } */

            TESlugs::insert([
                [
                    'key' => Str::slug($galery_title, '-'),
                    'reference_type'=> "TE\Gallery\Models\Gallery",
                    'reference_id'=> $item['galeriid'],
                    'prefix' => 'plugins.gallery::slug.galleries',
                    'created_at' => $item->created_at,
                    'updated_at' => $item->update_at
                ]
            ]);
            

        }



        /* foreach (TEVideos::all() as $key => $item) {
            
            TECategorieables::insert([
                [
                    'category_id' => 19,
                    'categorieable_type'=> "TE\Video\Models\Video",
                    'categorieable_id'=> $item->id,
                    'created_at' => $item->created_at,
                    'updated_at' => $item->update_at
                ]
            ]);
            
        } */

        // galeri - video - post - makale - category herşeyin slug tablosuna aktarılması lazım
        // İLGİLİ METODLARIN İÇİNE SLUG INSERTLERİ EKLE
       
       /* foreach (TEGalleries::all() as $key => $item) {

            TESlugs::insert([
                [
                    'key' => Str::slug($item['name'], '-'),
                    'reference_type'=> "TE\Gallery\Models\Gallery",
                    'reference_id'=> $item->id,
                    'created_at' => $item->created_at,
                    'updated_at' => $item->update_at
                ]
            ]);

        } */

        /* foreach (TEVideos::all() as $key => $item) {

            TESlugs::insert([
                [
                    'key' => Str::slug($item['name'], '-'),
                    'reference_type'=> "TE\Video\Models\Video",
                    'reference_id'=> $item->id,
                    'created_at' => $item->created_at,
                    'updated_at' => $item->update_at
                ]
            ]);
        } */
        //
        
    }

    public function instertVideo() {
        $character = ["Ý", "ý", "Þ", "þ", "ð"];
        $change = ["İ", "ı","Ş", "ş", "ğ"];
        
     

        foreach (Video::all() as $key => $item) {

            $video_title = str_replace($character, $change, $item['videotitle']);

            if($video_title) {

                if($item->videoembed) {
                    $parcala_embed = explode("embed/", $item->videoembed);
            
                    if(count($parcala_embed) > 1) {
                        $url = 'https://www.youtube.com/watch?v=' . strtok($parcala_embed[1], '"');
                        $mime_type = 'youtube';
                    }
                }

                if($item->videoname) {
                    $mime_type = 'video/mp4';
                    $url = $this->site.'/video/'.$item->video_name;
                } 


                TEMediaFiles::insert([
                    [
                        'id' => $item->id,
                        'user_id' => 1,
                        'name'=> $video_title,
                        'folder_id' => 0,
                        'options' => "",
                        'size' => 0,
                        'mime_type' => $mime_type,
                        'url' => $url,
                        'created_at' => $item['videodate'] ? $item['videodate'] : date('Y-m-d H:i:s'),
                        'updated_at' => $item['videodate'] ? $item['videodate'] : date('Y-m-d H:i:s')
                    ]
                ]);

                TEVideos::insert([
                    [
                        'name'=> $video_title,
                        'description' => $item['videosummary'],
                        'is_featured'=> 0,
                        'order' => 1,
                        'headline'=> 0,
                        'story'=> 0,
                        'image' => $item['videoimage'],
                        'hide_headline'=> 0,
                        'media_id'=> $item['videoid'],
                        'user_id' => 1,
                        'embed' => $item->videoembed,
                        'status' => 'published',
                        'created_at' => $item['videodate'] ? $item['videodate'] : date('Y-m-d H:i:s'),
                        'updated_at' => $item['videodate'] ? $item['videodate'] : date('Y-m-d H:i:s')
                    ]
                ]);
  
                TERedirections::insert([
                    [
                        'from' => '/video/'.Str::slug($video_title, '-').'/'.$item['videoid'],
                        'to' => '/'.Str::slug($video_title, '-'),
                        'status' => 'published'
                    ]
                ]); 

            

                TECategorieables::insert([
                    [
                        'category_id' => 19,
                        'categorieable_type'=> "TE\Video\Models\Video",
                        'categorieable_id'=> $item['videoid'],
                        'created_at' => $item['videodate'] ? $item['videodate'] : date('Y-m-d H:i:s'),
                        'updated_at' => $item['videodate'] ? $item['videodate'] : date('Y-m-d H:i:s')
                    ]
                ]);

                TESlugs::insert([
                    [
                        'key' => Str::slug($video_title, '-'),
                        'reference_type'=> "TE\Video\Models\Video",
                        'reference_id'=> $item['videoid'],
                        'created_at' => $item['videodate'] ? $item['videodate'] : date('Y-m-d H:i:s'),
                        'updated_at' => $item['videodate'] ? $item['videodate'] : date('Y-m-d H:i:s')
                    ]
                ]);

            }
        }
   
        
    }

    public function updateContents() {
        $data = TEContents::where('id', '>', 70000)->get();
        
        $character = ["&not;", "&LTR;", "&lrm;", 
        "&ndash;", 
        "&Ucirc;", 
        "&hellip;", 
        "&icirc;", 
        "&ucirc;", "&Acirc;", "&acirc;",
        "&lsquo;", "&#39;", "&rsquo;", 
        "&Ouml;", '&ouml;', 
        '&Ccedil;', "&ccedil;",
        '&Uuml;', "&uuml;",
        '&rdquo;', '&ldquo;', "&quot;",
        "&nbsp;"];
        
        $change = [" ", " ", 
        "-", 
        "U", 
        ".", 
        "i", 
        "u", "A", "a", 
        "'", "'", "'", 
        "Ö", 'ö', 
        'Ç', "ç",
        'Ü', "ü", 
        '"', '"', '"', 
        " "];

        foreach ($data as $key => $value) {
            /* $item = TEContents::find($value->id);
            
            

            if($item->content) {
                $item->content = str_replace($character, $change, $value->content);
                $item->save();
            } */


            $value->content = str_replace($character, $change, $value->content);
            $value->save();
        }
    }

    public function validateDate($date, $format = 'Y-m-d H:i:s')
    {
        $d = DateTime::createFromFormat($format, $date);
        return $d && $d->format($format) == $date;
    }

    public function insertHaber() {
        $character = ["&not;", "&LTR;", "&lrm;", 
        "&ndash;", 
        "&Ucirc;", 
        "&hellip;", 
        "&icirc;", 
        "&ucirc;", "&Acirc;", "&acirc;",
        "&lsquo;", "&#39;", "&rsquo;", 
        "&Ouml;", '&ouml;', 
        '&Ccedil;', "&ccedil;",
        '&Uuml;', "&uuml;",
        '&rdquo;', '&ldquo;', "&quot;",
        "&nbsp;", "&8217;", "&acute;"];
        
        $change = [" ", " ", 
        "-", 
        "U", 
        ".", 
        "i", 
        "u", "A", "a", 
        "'", "'", "'", 
        "Ö", 'ö', 
        'Ç', "ç",
        'Ü', "ü", 
        '"', '"', '"', 
        " ", "'", "'"];

        foreach (HaberHaberler::where("id", ">", 133416)->get() as $item) {

                $title = str_replace($character, $change, $item['hbaslik']);
                $title_slug = Str::slug($title, '-');
         
                
                
                  TESlugs::insert([
                        [
                            'key' => $title_slug,
                            'reference_type'=> 'TE\Blog\Models\Post',
                            'reference_id' => $item['id'],
                            'created_at' => $item['tarih']." ".$item['saat'],
                            'updated_at' => $item['tarih']." ".$item['saat'],
                        ]
                    ]);

                    TERedirections::insert([
                        [
                            'from' => '/'.$title_slug.'.html',
                            'to' => '/'.$title_slug,
                            'status' => 'published'
                        ]
                    ]); 
                 
                    TEContents::insert([
                        [
                            'reference_type' => 'TE\Blog\Models\Post',
                            'reference_id' => $item['id'],
                            'content' => trim(html_entity_decode(str_replace($character, $change, $item['hdetay']))),
                            'created_at' => $item['tarih']." ".$item['saat'],
                            'updated_at' => $item['tarih']." ".$item['saat'],
                        ]
                    ]); 

                    TECategorieables::insert([
                        [
                            'category_id' =>  $item['kategori'] ? $item['kategori'] : 0,
                            'categorieable_type'=> 'TE\Blog\Models\Post', 
                            'categorieable_id'=> $item['id'],
                            'created_at' => $item['tarih']." ".$item['saat'],
                            'updated_at' => $item['tarih']." ".$item['saat'],
                        ]
                    ]); 

                    TEPosts::insert([
                        [
                            'id'=> $item['id'],
                            'name' =>  $title,
                            'author_type'=> 'TE\Users\Models\User', 
                            'author_id'=> ($item['editor'] && is_numeric($item['editor'])) ? $item['editor'] : 0,
                            'status' => $item['onay'] ? 'published' : 'draft',
                            'created_at' => $item['tarih']." ".$item['saat'],
                            'updated_at' => $item['tarih']." ".$item['saat'],
                        ]
                    ]);

                   TEPostProperties::insert([
                        [
                            'post_id' =>  $item['id'],
                            'image'=> $item['resim'] ? $this->site.'/image/'.$item['resim'] : '',
                            'description'=> str_replace($character, $change, $item['hkisa']),
                            'embed'=> $item['reshiza'],
                            'position' => '["headline"]', // hem nullable değil hem de default değeri yok
                            'created_at' => $item['tarih']." ".$item['saat'],
                            'updated_at' => $item['tarih']." ".$item['saat'],
                        ]
                    ]);

        }
    }

    // MAKALE
    public function insertArticles() {

        $character = ["&not;", "&LTR;", "&lrm;", 
        "&ndash;", 
        "&Ucirc;", 
        "&hellip;", 
        "&icirc;", 
        "&ucirc;", "&Acirc;", "&acirc;",
        "&lsquo;", "&#39;", "&rsquo;", 
        "&Ouml;", '&ouml;', 
        '&Ccedil;', "&ccedil;",
        '&Uuml;', "&uuml;",
        '&rdquo;', '&ldquo;', "&quot;",
        "&nbsp;", "&8217;", "&acute;"];
        
        $change = [" ", " ", 
        "-", 
        "U", 
        ".", 
        "i", 
        "u", "A", "a", 
        "'", "'", "'", 
        "Ö", 'ö', 
        'Ç', "ç",
        'Ü', "ü", 
        '"', '"', '"', 
        " ", "'", "'"];

        foreach (HaberMakale::all() as $item) {
            
            $title = str_replace($character, $change, $item['baslik']);
            $title_slug = Str::slug($title, '-');

           TERedirections::insert([
                [
                    'from' => '/m_'.$item['id'].'/'.$title_slug.'/',
                    'to' => '/'.$title_slug,
                    'status' => 'published'
                ]
            ]); 

           TESlugs::insert([
                [
                    'key' =>  $title_slug,
                    'reference_type'=> 'TE\Authors\Models\Article',
                    'reference_id' => $item['id'],
                    'created_at' => $item['tarih']." ".$item['saat'],
                    'updated_at' => $item['tarih']." ".$item['saat'],
                ]
            ]);
            
            TEArticles::insert([
                [
                    'id' => $item['id'],
                    'author_id'=> $item['editor'] ? $item['editor'] : 0,
                    'user_id'=> 1,
                    'name' =>  $title,
                    'description' => "",
                    'status' => 'published',
                    'created_at' => $item['tarih']." ".$item['saat'],
                    'updated_at' => $item['tarih']." ".$item['saat'],

                ]
            ]);

            TEContents::insert([
                [
                    'reference_type' => 'TE\Authors\Models\Article',
                    'reference_id' => $item['id'],
                    'content' => trim(html_entity_decode(str_replace($character, $change, $item['detay']))),
                    'created_at' => $item['tarih']." ".$item['saat'],
                    'updated_at' => $item['tarih']." ".$item['saat'],
                ]
            ]);
        }

    }

    // Yorumlar
    public function insertComments(){
        $character = ["Ý", "ý", "Þ", "þ", "ð"];
        $change = ["İ", "ı","Ş", "ş", "ğ"];

        

        foreach (HaberYorumlar::all() as $item) {
            $yorum = str_replace($character, $change, $item['yorumyorum']);
            $yorumad = str_replace($character, $change, $item['yorumad']);
            if($yorumad) {

                TEComments::insert([
                    [
                        'id'=> $item['yorumid'],
                        'name'=> $yorumad,
                        'body' => $yorum,
                        'reference_type' => 'TE\Blog\Models\Post',
                        'reference_id' => $item['yorumid'],
                        'ip' => $item['yorumip'] ? $item['yorumip'] : 0,
                        'status' => $item['yorumonay'],
                        'created_at' => $item['yorumtarih']." ".$item['yorumsaat'],
                        'updated_at' => $item['yorumtarih']." ".$item['yorumsaat']
                    ]
                ]);

                TECommentsLikedIP::insert([
                    [
                        'comment_id'=> $item['yorumid'],
                        'ip' => $item['yorumip'] ? $item['yorumip'] : 0,
                    ]
                ]);

            
            }
        }
    }

}