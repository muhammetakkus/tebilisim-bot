<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TECategorieables extends Model
{
    use HasFactory;
    protected $connection = 'mysql2';
    protected $table = "TE_categorieables";
}
