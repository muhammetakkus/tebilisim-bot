<?php

namespace App\Models\Ticarihayat;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class HaberKategori extends Model
{
    use HasFactory;
    protected $table = "kategori";
}
