<?php

namespace App\Models\Ticarihayat;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class HaberYazarlar extends Model
{
    use HasFactory;
    protected $table = "yazar";
}
