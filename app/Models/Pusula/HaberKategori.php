<?php

namespace App\Models\Pusula;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class HaberKategori extends Model
{
    use HasFactory;
    protected $table = "xhnsx_hkategori";
}
