<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class HaberYazarlar extends Model
{
    use HasFactory;
    protected $table = "yedek_yazarlar";
}
