<?php

namespace App\Models\Ondorthaber;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class HaberYorumlar extends Model
{
    use HasFactory;

    protected $table = "yorumlar";
}
