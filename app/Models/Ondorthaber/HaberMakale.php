<?php

namespace App\Models\Ondorthaber;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class HaberMakale extends Model
{
    use HasFactory;
    protected $table = "article";
}
