<?php

namespace App\Models\Ondorthaber;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class HaberGallery extends Model
{
    use HasFactory;

    protected $table = "galery";
}
